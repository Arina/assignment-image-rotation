#include <stdio.h>
#include <stdlib.h>

#include "image.h"
#include "bmp_header.h"

struct image * image(){
   return (struct image *) malloc(sizeof(struct image));
}

struct pixel * imagedata(struct image const image){
   return (struct pixel *) malloc(image.height * image.width * sizeof(struct pixel));
}

struct image rotate(struct image const source) {
    struct image * result = image();
    result->height = source.width;
    result->width = source.height;

    result->data = imagedata(source);

    for (uint64_t line = 0; line < source.height; line++) {
        for (uint64_t c = 0; c < source.width; c++) {
            result->data[((result->height - c - 1) * result->width) + line] = source.data[(line * source.width) + c];
        }
    }
    return *result;
}

void free_image(struct image* image){
    free(image->data);
    free(image);
}
