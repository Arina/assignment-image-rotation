#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "file.h"

enum open_status file_open(FILE ** file, const char * path, const char * mode){
    *file = fopen(path, mode);
    if (!*file) {
        if(mode[0] == 'r') return OPEN_ERROR_READ;
        if(mode[0] == 'w') return OPEN_ERROR_WRITE;
    }
    return OPEN_OK;
}

enum close_status file_close(FILE * file){
    fclose(file);
    return CLOSE_OK;
}

void print_open_status(enum open_status status){
    if(status >= OPEN_OK && status <= OPEN_ERROR_WRITE) printf("%s\n", open_status_descr[status]);
    else printf("ERROR: Undefined file open error\n");
}

void print_close_status(enum close_status status){
    if(status == CLOSE_OK) printf("%s\n", close_status_descr[status]);
    else printf("ERROR: Undefined file open error\n");
}
