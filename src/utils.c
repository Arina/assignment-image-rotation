#include <stdio.h>
#include <stdlib.h>

#include "bmp_header.h"
#include "bmp.h"
#include "image.h"
#include "file.h"

int process(int argc, char *argv[]){

    const char * source;
    const char * dest;
    if(argc == 3){
        source = argv[1];
        dest = argv[2];
    } else if(argc == 2){
        source = argv[1];
        dest = "output.bmp";
    } else {
        source = "input.bmp";
        dest = "output.bmp";
    }

    FILE * in, * out;

    {
        enum open_status status = file_open(&in, source, "rb");
        print_open_status(status);
        if(status) return status;
    }

    struct image * img = image();

    {
        enum read_status status = from_bmp(in, img);
        print_read_status(status);
        if(status) return status;
    }

    {
        enum close_status status = file_close(in);
        print_close_status(status);
        if(status) return status;
    }

    struct image result = rotate(*img);

    {
        enum open_status status = file_open(&out, dest, "wb");
        print_open_status(status);
        if(status) return status;
    }

    {
        enum write_status status = to_bmp(out, &result);
        print_write_status(status);
        if(status) return status;
    }

    {
        enum close_status status = file_close(out);
        print_close_status(status);
        if(status) return status;
    }

    free_image(img);

    return 0;
}
