#ifndef FILE_H_INCLUDED
#define FILE_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR_READ,
    OPEN_ERROR_WRITE,
};

enum open_status file_open(FILE ** file, const char * path, const char * mode);

static const char* const open_status_descr[] = {
    [OPEN_OK] = "OK: File has been opened successfully",
    [OPEN_ERROR_READ] = "ERROR: File hasn't been opened for reading",
    [OPEN_ERROR_WRITE] = "ERROR: File hasn't been opened for writing"
};

void print_open_status(enum open_status status);

enum close_status {
    CLOSE_OK = 0,
};

enum close_status file_close(FILE * file);

static const char* const close_status_descr[] = {
    [CLOSE_OK] = "OK: File has been closed successfully"
};

void print_close_status(enum close_status status);

#endif
