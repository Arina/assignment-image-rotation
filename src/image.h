#ifndef IMAGE_H_INCLUDED
#define IMAGE_H_INCLUDED

#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image rotate(struct image const source);

struct image * image();
struct pixel * imagedata(struct image const image);
void free_image(struct image* image);

#endif
