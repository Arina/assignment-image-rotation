#ifndef BMP_H_INCLUDED
#define BMP_H_INCLUDED

#include <stdio.h>

#include "image.h"
#include "bmp_header.h"


#define BF_TYPE 0x4D42
#define BI_SIZE 40
#define BI_BIT_COUNT 24

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_NO_DATA,
    READ_ERROR
};

enum read_status from_bmp( FILE* in, struct image* img);

static const char* const read_status_descr[] = {
    [READ_OK] = "OK: Image has been loaded successfully",
    [READ_INVALID_HEADER] = "ERROR: Invalid image file header",
    [READ_INVALID_BITS] = "ERROR: Invalid bits, must be 24-bit image",
    [READ_INVALID_SIGNATURE] = "ERROR: Invalid image file signature",
    [READ_NO_DATA] = "ERROR: No image file provided",
    [READ_ERROR] = "ERROR: Unable to read from image file"
};

void print_read_status(enum read_status status);

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img );

static const char* const write_status_descr[] = {
    [WRITE_OK] = "OK: Image has been written to file",
    [WRITE_ERROR] = "ERROR: Unable to write image to file"
};

void print_write_status(enum write_status status);

struct bmp_header new_bmp_header(struct image const * image);
uint64_t get_padding(struct image const * image);
enum read_status validate_header(struct bmp_header header);

#endif


