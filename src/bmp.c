#include <stdlib.h>
#include <stdio.h>

#include "bmp_header.h"
#include "bmp.h"
#include "image.h"

enum read_status validate_header(struct bmp_header header){
    if(header.bfType != BF_TYPE) return READ_INVALID_SIGNATURE;
    if(header.biBitCount != BI_BIT_COUNT) return READ_INVALID_BITS;
    if(header.biCompression != 0 || header.bfileSize != header.bOffBits + header.biSizeImage) return READ_INVALID_HEADER;
    return READ_OK;
};

uint64_t get_padding(struct image const * image){
    return image->width % 4;
}

struct bmp_header new_bmp_header(struct image const * image) {
    struct bmp_header header = {0};
    header.bfType = BF_TYPE;
    header.bfileSize = image->width * image->height * sizeof(struct pixel) + image->height * (image->width % 4) + sizeof(struct bmp_header);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BI_SIZE;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = 1;
    header.biBitCount = BI_BIT_COUNT;
    header.biCompression = 0;
    header.biSizeImage = header.bfileSize - header.bOffBits;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

void print_bmp_header(struct bmp_header * header){
    printf("BMP Header:\n");
    printf("header->bfType %d\n", header->bfType);
    printf("header->bfileSize %d\n", header->bfileSize);
    printf("header->bfReserved %d\n", header->bfReserved);
    printf("header->bOffBits %d\n", header->bOffBits);
    printf("header->biSize %d\n", header->biSize);
    printf("header->biWidth %d\n", header->biWidth);
    printf("header->biHeight %d\n", header->biHeight);
    printf("header->biPlanes %d\n", header->biPlanes);
    printf("header->biBitCount %d\n", header->biBitCount);
    printf("header->biCompression %d\n", header->biCompression);
    printf("header->biSizeImage %d\n", header->biSizeImage);
    printf("header->biXPelsPerMeter %d\n", header->biXPelsPerMeter);
    printf("header->biYPelsPerMeter %d\n", header->biYPelsPerMeter);
    printf("header->biClrUsed %d\n", header->biClrUsed);
    printf("header->biClrImportant %d\n", header->biClrImportant);
}

enum read_status from_bmp( FILE* in, struct image* img){
    if(in == NULL) return READ_NO_DATA;
    struct bmp_header img_header;
    fread(&img_header, 1, sizeof(struct bmp_header), in);
    print_bmp_header(&img_header);
    if(validate_header(img_header)) return validate_header(img_header);
    uint64_t img_padding = get_padding(img);
    img->data = imagedata(*img);
    img->height = img_header.biHeight;
    img->width = img_header.biWidth;
    fseek(in, img_header.bOffBits, SEEK_SET);
    for (uint32_t line = 0; line < img_header.biHeight; line++) {
        if(!fread(&(img->data[line * img_header.biWidth]), sizeof(struct pixel), img->width, in)) return READ_ERROR;
        fseek(in, img_padding, SEEK_CUR);
    }
    return READ_OK;
}

void print_read_status(enum read_status status){
    if(status >= READ_OK && status <= READ_ERROR) printf("%s\n", read_status_descr[status]);
    else printf("ERROR: Undefined reading error\n");
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    struct bmp_header img_header = new_bmp_header(img);
    uint64_t img_padding = get_padding(img);
    struct pixel placeholder = {0};
    if(!fwrite(&img_header, 1, sizeof(struct bmp_header), out)) return WRITE_ERROR;
    for (uint64_t line = 0; line < img->height; line++) {
        if(!fwrite(&img->data[line * img->width], sizeof(struct pixel), img->width, out)) return WRITE_ERROR;
        if(img_padding) {
            if(!fwrite(&placeholder, 1, img_padding, out)) return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

void print_write_status(enum write_status status){
    if(status >= WRITE_OK && status <= WRITE_ERROR) printf("%s\n", write_status_descr[status]);
    else printf("ERROR: Undefined writing error\n");
}
